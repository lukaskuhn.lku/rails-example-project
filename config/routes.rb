Rails.application.routes.draw do
  
  devise_for :users, controllers: { registrations: "registrations" }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

 unauthenticated do
  as :user do
    root :to => 'pages#home'
  end
 end
 
  authenticated do
      root :to => 'products#index'
  end

  root 'pages#home'
  get 'dashboard' => 'pages#dashboard'

  get 'contact' => 'pages#contact'

  get 'shopify/auth' => 'shopifys#auth'
  get 'shopify/install' => 'shopifys#install'


  resources :products
  resources :categories
  resources :charges

  get 'affiliate/home', to: 'affiliates#home'

  get 'unsubscribe', to: 'charges#endsubscription'

  get 'subscribe', to: 'charges#new'
  get 'profile', to: 'pages#profile'

  resources :users, except: [:new]
  resources :influencers 
  resources :audiences
  resources :faqs
  resources :engagements

  get 'pricing', to: 'pages#pricing'
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'

  get 'admin', to: 'pages#admin'
end
