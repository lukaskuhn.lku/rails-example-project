module EngagementsHelper
    
    def calc_heroscore(engagement)
        if calc_dayscore(engagement.post_date) > 0 
            hero_points = calc_points(engagement) / calc_dayscore(engagement.post_date)
        else 
            hero_points = calc_points(engagement) / 1
        end
        if hero_points <= 30
            hero_score = (hero_points / 10).to_f
        else
            if hero_points > 30 && hero_points <= 280
                hero_score = ((((hero_points - 30).to_f) / 5 + 30) / 10).round(1)
            else 
                if hero_points > 280 && hero_points <= 380
                    hero_score = (((hero_points - 280).to_f) / 10 / 10 + 8.0).round(1)
                else
                    if hero_points > 380 && hero_points <= 780
                        hero_score = (((hero_points - 380).to_f) / 40 / 10 + 9.0).round(1)
                    else 
                        hero_score = 10
                    end
                end
            end 
        end
        return hero_score
    end
    
    
    
    def calc_dayscore(post_date)
        days_between = (Date.today - post_date.to_date).to_i
        if days_between >= 52
            day_score = 127 + (days_between - 52)
        else 
            if days_between >= 14
                day_score = 51 + (2 * (days_between - 14))
            else 
                if days_between >= 6
                    day_score = 27 + (3 * (days_between - 6))
                else 
                    if days_between >= 3
                        day_score = 15 + (4 * (days_between - 3))
                    else 
                        day_score = days_between * 5
                    end 
                end 
            end 
        end
        return day_score
    end
    
    
    def calc_points(engagement)
        points = ((engagement.likes * 0.5) + engagement.comments + (engagement.shares * 0.75)).to_i
        return points
    end
    
    def calc_like_score(engagement)
        maximum_points = 780 * calc_dayscore(engagement.post_date)
        optimal_likes = (maximum_points / 3) * 2
        if engagement.likes > optimal_likes 
            like_score = 10.0
        else
            like_score = ((engagement.likes.to_f / optimal_likes) * 10).round(1)
        end
        return like_score
    end
    
    def calc_comment_score(engagement)
        maximum_points = 780 * calc_dayscore(engagement.post_date)
        optimal_comments = (maximum_points / 3)
        if engagement.comments > optimal_comments
            comment_score = 10.0
        else
            comment_score = ((engagement.comments.to_f / optimal_comments) * 10).round(1)
        end
        return comment_score
    end
    
    def calc_share_score(engagement)
        maximum_points = 780 * calc_dayscore(engagement.post_date)
        optimal_shares = (maximum_points / 3) * 1.33333333333333
        if engagement.shares > optimal_shares
            share_score = 10.0
        else
            share_score = ((engagement.shares.to_f / optimal_shares) * 10).round(1)
        end
        return share_score
    end
        

end