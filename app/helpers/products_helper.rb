module ProductsHelper
    
    def calc_heroscore(product)
        hero_points = calc_points(product) / calc_dayscore(product.facebookad_posted_at)
        if hero_points <= 30
            hero_score = (hero_points / 10).to_f
        else
            if hero_points > 30 && hero_points <= 280
                hero_score = ((((hero_points - 30).to_f) / 5 + 30) / 10).round(1)
            else 
                if hero_points > 280 && hero_points <= 380
                    hero_score = (((hero_points - 280).to_f) / 10 / 10 + 8.0).round(1)
                else
                    if hero_points > 380 && hero_points <= 780
                        hero_score = (((hero_points - 380).to_f) / 40 / 10 + 9.0).round(1)
                    else 
                        hero_score = 10
                    end
                end
            end 
        end
        return hero_score
    end
    
    
    
    def calc_dayscore(post_date)
        days_between = (Date.today - post_date.to_date).to_i
        if days_between >= 52
            day_score = 127 + (days_between - 52)
        else 
            if days_between >= 14
                day_score = 51 + (2 * (days_between - 14))
            else 
                if days_between >= 6
                    day_score = 27 + (3 * (days_between - 6))
                else 
                    if days_between >= 3
                        day_score = 15 + (4 * (days_between - 3))
                    else 
                        day_score = days_between * 5
                    end 
                end 
            end 
        end
        return day_score
    end
    
    
    def calc_points(product)
        points = ((product.likes * 0.5) + product.comments + (product.shares * 0.75)).to_i
        return points
    end

end