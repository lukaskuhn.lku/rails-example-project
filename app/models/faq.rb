class Faq < ApplicationRecord
    validates :question , presence: true
    validates :answer, presence: true
    validates :sortId, presence: true, numericality: { only_integer: true }
end
