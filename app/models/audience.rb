class Audience < ApplicationRecord
    has_many :audience_categories
    has_many :categories, through: :audience_categories
end