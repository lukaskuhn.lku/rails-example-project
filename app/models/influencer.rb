class Influencer < ApplicationRecord
    has_many :influencer_categories
    has_many :categories, through: :influencer_categories
end