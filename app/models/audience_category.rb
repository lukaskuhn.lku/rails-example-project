class AudienceCategory < ApplicationRecord
    belongs_to :audience
    belongs_to :category
end