class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  scope :latest, -> { where("created_at >= ?", Time.now - 30.days) }
  scope :before30, -> { where("created_at >= ? AND created_at <= ?", Time.now - 60.days, Time.now - 30.days) }

end
