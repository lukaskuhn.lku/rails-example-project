class Engagement < ApplicationRecord
    validates :likes, presence: true, numericality: { only_integer: true }
    validates :comments, presence: true, numericality: { only_integer: true }
    validates :shares, presence: true, numericality: { only_integer: true }
    validates :post_date, presence: true
end
