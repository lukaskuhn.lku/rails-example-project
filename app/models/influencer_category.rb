class InfluencerCategory < ApplicationRecord
    belongs_to :influencer
    belongs_to :category
end