class Product < ApplicationRecord
    has_many :product_categories
    has_many :categories, through: :product_categories
    scope :next, lambda {|id| where("id > ?",id).order("id ASC") } # this is the default ordering for AR
    scope :previous, lambda {|id| where("id < ?",id).order("id DESC") }
    validates :likes, presence: true, numericality: { only_integer: true }
    validates :comments, presence: true, numericality: { only_integer: true }
    validates :shares, presence: true, numericality: { only_integer: true }

    def next
      Product.next(self.id).first
    end

    def previous
      Product.previous(self.id).first
    end
end