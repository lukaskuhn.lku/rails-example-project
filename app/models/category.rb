class Category < ApplicationRecord
    has_many :product_categories
    has_many :products, through: :product_categories

    has_many :influencer_categories
    has_many :influencers, through: :influencer_categories

    has_many :audience_categories
    has_many :audiences, through: :audience_categories
end