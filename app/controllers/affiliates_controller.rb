class AffiliatesController < ApplicationController

    def home
        if user_signed_in?
            @ref_id = (current_user.id + 1000); 
            @affiliates_on_this_user = Affiliate.where(:ref => @ref_id)
        end
    end

end