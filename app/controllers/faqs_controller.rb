class FaqsController < ApplicationController

    #before_action :authenticate_user!
    #before_action :is_user_subscribed
    
    def index
        @faqs = Faq.all.order('"sortId" ASC')
    end

    def new
        if current_user.admin?
            @faq = Faq.new
        else
            redirect_to root_path
        end

    end

    def create 
        if  current_user.admin?
            @faq = Faq.new(faq_params)
            if @faq.save
                redirect_to faqs_path
            else
                render 'new'
            end
        else
            redirect_to root_path
        end
    end

    def update
        if  current_user.admin?
            set_faq
            if @faq.update(faq_params)
                redirect_to faqs_path
            else
                render 'edit' 
            end
        else
            redirect_to root_path
        end
    end

    def edit
        if  current_user.admin?
          set_faq
        else
            redirect_to root_path
        end
    end

    def show 

    end

    def destroy
        set_faq
        @faq.delete

        redirect_to faqs_path
    end

    private
        def faq_params
            params.require(:faq).permit(:question,:answer,:sortId)
        end

        def set_faq
            @faq = Faq.find(params[:id])
        end

        def is_user_subscribed
            if !current_user.subscribed? and !current_user.admin?
                redirect_to subscribe_path
            end
        end

        
end