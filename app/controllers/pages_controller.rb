
class PagesController < ApplicationController

    def home
        if user_signed_in?
            if current_user.subscribed? or current_user.admin?
                redirect_to products_path
            else
                redirect_to subscribe_path
            end
        end

    end

    def admin
        if user_signed_in?
            if current_user.admin?

                @users = User.all
                @users_last_30 = User.latest
                @users_before_30 = User.before30

                if User.before30.count >= User.latest.count
                    @userchange = -1 * (((User.before30.count - User.latest.count)/User.before30.count)*100)
                else
                    @userchange = ((User.latest.count - User.before30.count)/User.before30.count)*100
                end
            else
                redirect_to root_path
            end
        end
    end

    def dashboard
        if current_user.admin?
              @usercount = User.count
        else
            redirect_to root_path
        end
    end

    def profile
        @registered_user_count = User.count

        if user_signed_in?
            @user = current_user


            if @user.subscribed? and @user.subscriptionid != nil

                @subscription = Stripe::Subscription.retrieve(@user.subscriptionid)

                if @subscription.status == "trialing"
                    @trial = true
                end

                if @subscription.status == "active"
                    @active = true
                end

                if @subscription.status == "canceled"
                    @canceled = true
                end
            end
        end
    end

    def faq
    end

    def contact
    end

end