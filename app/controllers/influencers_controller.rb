class InfluencersController < ApplicationController

    before_action :authenticate_user!
    #before_action :is_user_subscribed

    def create 
        @influencer = Influencer.new(influencer_params)
        if @influencer.save
            redirect_to root_path
        else
            render 'new'
        end
    end

    
    def index 
        @title = "All Winning Products"
        @categories = Category.all.order('name ASC')

        @products = Product.all
        @products = @products.sort { |a,b|  a.id <=> b.id }
       
        @products = @products.reverse
    end 
    
    def show
            set_influencer
    end

    def destroy 
        set_product
        @product.destroy
        redirect_to root_path 
    end

    def update
        set_influencer
        if @influencer.update(influencer_params)
            redirect_to influencer_path(@influencer)
        else
            render 'edit' 
        end
    end

    def new
      if current_user.admin?
             @influencer = Influencer.new
        else
            redirect_to root_path
        end
    end

    def edit
        set_influencer
    end

    private 
        def set_product
            @product = Product.find(params[:id])
        end

        def set_influencer
            @influencer = Influencer.find(params[:id])
        end

        # def is_user_subscribed
        #    if !current_user.subscribed? and !current_user.admin?
        #             redirect_to subscribe_path
        #     end
        #
        # end

        def influencer_params 
            params.require(:influencer).permit(:name, :ighandle, :follower, :imageurl, :engagement, :shoutoutprice, :likesperpost, :commentsperpost, :country, category_ids: [])
        end
end