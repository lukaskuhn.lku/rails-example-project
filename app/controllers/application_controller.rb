class ApplicationController < ActionController::Base

    protect_from_forgery with: :exception

    before_action :configure_permitted_parameters, if: :devise_controller?

    private
        def is_user_still_subscribed
            if user_signed_in?
                if current_user.subscribed?
                    if current_user.subscribed_until?
                        if (Time.at(current_user.subscribed_until).to_i - Time.now.to_i) < 0
                            current_user.subscribed = false
                            current_user.save!
                        end
                    end
                end
            end
        end

        protected
            def configure_permitted_parameters
                devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:name, :email, :paypal, :password) }
                devise_parameter_sanitizer.permit(:account_update) { |u| u.permit(:name, :email, :paypal, :password, :current_password) }
            end

end
