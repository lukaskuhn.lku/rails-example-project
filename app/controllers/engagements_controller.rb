class EngagementsController < ApplicationController
    include EngagementsHelper
    # before_action :authenticate_user!
    # before_action :is_user_subscribed
    
    
    
    def index
        redirect_to root_path
    end
    def new
        @engagement = Engagement.new
    end
    
    def create
        @engagement = Engagement.new(engagement_params)
        @engagement.heroscore = calc_heroscore(@engagement)
        if @engagement.save
            redirect_to engagement_path(@engagement)
        else
            render 'new'
        end 
    end
    
    def show
        if Engagement.exists?(params[:id])
            @engagement = Engagement.find(params[:id])
            @engagement.destroy
        else 
            redirect_to new_engagement_path
        end
    end
    
    private
    
        def is_user_subscribed
            if !current_user.subscribed? and !current_user.admin?
                redirect_to subscribe_path
            end
        end
        
        def engagement_params 
            params.require(:engagement).permit(:likes, :shares, :comments, :post_date)
        end
    
end
    
