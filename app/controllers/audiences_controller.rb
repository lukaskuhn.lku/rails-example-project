class AudiencesController < ApplicationController

    before_action :authenticate_user!
    #before_action :is_user_subscribed

    def create 
        @audience = Audience.new(influencer_params)
        if @audience.save
            redirect_to root_path
        else
            render 'new'
        end
    end

    
    def index 
        @title = "All Winning Products"
        @categories = Category.all.order('name ASC')

        @products = Product.all
        @products = @products.sort { |a,b|  a.id <=> b.id }
       
        @products = @products.reverse
    end 
    
    def show
            set_product
            if @product.audiences
                @audiences = @product.audiences.split(',')
            end
            if @product.influencer
                @influencer = @product.influencer.split(',')
            end
             
            @profit = (@product.sellprice - @product.storeprice)
    end

    def destroy 
        set_product
        @product.destroy
        redirect_to root_path 
    end

    def update
        set_product
        if @product.update(product_params)
            redirect_to product_path(@product)
        else
            render 'edit' 
        end
    end

    def new
      if current_user.admin?
             @audience = Audience.new
        else
            redirect_to root_path
        end
    end

    def edit
        set_product
    end

    private 
        def set_product
            @product = Product.find(params[:id])
        end

        def influencer_params 
            params.require(:audience).permit(:name, :size, :woman, :man, category_ids: [])
        end
end