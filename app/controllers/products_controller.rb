
class ProductsController < ApplicationController
    include ProductsHelper
    require 'will_paginate/array'
    before_action :authenticate_user!
    before_action :is_user_subscribed
    

    def create
        @product = Product.new(product_params)
        @product.heroscore = calc_heroscore(@product)
        if @product.save
            redirect_to product_path(@product)
        else
            render 'new'
        end 
    end

    def index 
        @title = "All Winning Products"
        @categories = Category.all.order('name ASC')
        
        #@products = Product.all
        #@products = Product.search(params[:search]).order("#{sort_column} #{sort_direction}").paginate(:page => params[:products_page], :per_page => 5)
        #@products = @products.sort { |a,b|  a.id <=> b.id }
        #@products = @products.reverse
        @products = Product.paginate(page: params[:page], per_page: 16).order('id DESC')
    end 
    
    def show
            set_product 
            if @product.audiences
                @audiences = @product.audiences.split(',')
            end
            if @product.influencer
                @influencer = @product.influencer.split(',')
            end
             
            @profit = (@product.sellprice - @product.storeprice)

            @categories = @product.categories
    end

    def destroy 
        set_product
        @product.destroy
        redirect_to root_path 
    end

    def update
        set_product
        @product.heroscore = calc_heroscore(@product)
        if @product.update(product_params)
            redirect_to product_path(@product)
        else
            render 'edit' 
        end
    end

    def pricing

    end

    def new
        if current_user.admin?
            @product = Product.new
        else
            redirect_to root_path
        end
    end

    def edit
        set_product
    end

    private 
        def set_product
            @product = Product.find(params[:id])
        end
        def is_user_subscribed
            if !current_user.subscribed? and !current_user.admin?
                 redirect_to subscribe_path
            end
        end

        def product_params 
            params.require(:product).permit(:title, :img, :buyprice, :sellprice, :description, :adcopy, :audiences, :videoadurl, :imageadurl, :influencer, :instagramadcopy, :freeplusshipping, :storename, :storeurl, :storeprice, :storefeedbackscore, :storeepacketprice, :storeepacket, :heroscore, :likes, :shares, :comments, :facebookad_posted_at, :facebookad_url, :review_csv_url, :product_rating, :product_page_url, :facebook_page_url, :facebook_female, :facebook_male, :active, :trends_keyword, :amazonurl, :ebayurl, :oberloimporturl, :youtubevideoadurl, category_ids: [])
        end
end