class CreateInfluencer < ActiveRecord::Migration[5.2]
  def change
    create_table :influencers do |t|
      t.string :name
      t.string :ighandle
      t.decimal :follower
      t.string :imageurl
      t.timestamps
    end
  end
end
