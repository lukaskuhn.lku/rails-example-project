class AddCountryInfluencers < ActiveRecord::Migration[5.2]
  def change
    add_column :influencers, :country, :string
  end
end
