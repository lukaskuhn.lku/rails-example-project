class CreateEngagements < ActiveRecord::Migration[5.2]
  def change
    create_table :engagements do |t|
      t.integer :likes
      t.integer :comments
      t.integer :shares
      t.datetime :post_date
      t.timestamps
    end
  end
end
