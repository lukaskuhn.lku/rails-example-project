class CreateAudienceCategory < ActiveRecord::Migration[5.2]
  def change
    create_table :audience_categories do |t|
      t.integer :audience_id
      t.integer :category_id
    end
  end
end
