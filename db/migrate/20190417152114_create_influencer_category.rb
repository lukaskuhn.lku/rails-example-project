class CreateInfluencerCategory < ActiveRecord::Migration[5.2]
  def change
    create_table :influencer_categories do |t|
      t.integer :influencer_id
      t.integer :category_id
    end
  end
end
