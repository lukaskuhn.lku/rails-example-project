class AddInfoToInfluencer < ActiveRecord::Migration[5.2]
  def change
    add_column :influencers, :engagement, :decimal
    add_column :influencers, :shoutoutprice, :decimal
    add_column :influencers, :likesperpost, :int
    add_column :influencers, :commentsperpost, :int
  end
end
