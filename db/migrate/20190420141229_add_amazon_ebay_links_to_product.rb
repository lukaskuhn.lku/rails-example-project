class AddAmazonEbayLinksToProduct < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :amazonurl, :string
    add_column :products, :ebayurl, :string
  end
end
