class AddPendingToAffiliates < ActiveRecord::Migration[5.2]
  def change
    add_column :affiliates, :pending, :boolean, default: false
  end
end
