class CreateAudiences < ActiveRecord::Migration[5.2]
  def change
    create_table :audiences do |t|
      t.string :name
      t.integer :size
      t.integer :woman
      t.integer :man
      t.timestamps
    end
  end
end
