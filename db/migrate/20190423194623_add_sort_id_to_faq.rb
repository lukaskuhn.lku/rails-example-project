class AddSortIdToFaq < ActiveRecord::Migration[5.2]
  def change
    add_column :faqs, :sortId, :int
  end
end
