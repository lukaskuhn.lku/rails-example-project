class AddHeroscoreToEngagement < ActiveRecord::Migration[5.2]
  def change
    add_column :engagements, :heroscore, :decimal
  end
end
