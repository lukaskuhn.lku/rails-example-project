class CreateWarmProduct < ActiveRecord::Migration[5.2]
  def change
    create_table :warm_products do |t|
      t.timestamps
    end
  end
end
