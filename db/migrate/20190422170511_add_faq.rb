class AddFaq < ActiveRecord::Migration[5.2]
  def change
    add_column :faqs, :question, :text
    add_column :faqs, :answer, :text
  end
end
