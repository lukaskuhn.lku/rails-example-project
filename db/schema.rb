# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_13_081156) do

  create_table "affiliates", force: :cascade do |t|
    t.string "ref"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
    t.boolean "pending", default: false
  end

  create_table "audience_categories", force: :cascade do |t|
    t.integer "audience_id"
    t.integer "category_id"
  end

  create_table "audiences", force: :cascade do |t|
    t.string "name"
    t.integer "size"
    t.integer "woman"
    t.integer "man"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "engagements", force: :cascade do |t|
    t.integer "likes"
    t.integer "comments"
    t.integer "shares"
    t.datetime "post_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "heroscore"
  end

  create_table "faqs", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "question"
    t.text "answer"
    t.integer "sortId"
  end

  create_table "influencer_categories", force: :cascade do |t|
    t.integer "influencer_id"
    t.integer "category_id"
  end

  create_table "influencers", force: :cascade do |t|
    t.string "name"
    t.string "ighandle"
    t.decimal "follower"
    t.string "imageurl"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "engagement"
    t.decimal "shoutoutprice"
    t.integer "likesperpost"
    t.integer "commentsperpost"
    t.string "country"
  end

  create_table "product_categories", force: :cascade do |t|
    t.integer "product_id"
    t.integer "category_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "title"
    t.string "img"
    t.decimal "sellprice"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
    t.text "adcopy"
    t.string "videoadurl"
    t.string "pictureadurl"
    t.text "audiences"
    t.text "influencer"
    t.boolean "freeplusshipping"
    t.text "instagramadcopy"
    t.string "storename"
    t.string "storeurl"
    t.float "storefeedbackscore"
    t.float "storeprice"
    t.boolean "storeepacket"
    t.float "storeepacketprice"
    t.integer "likes"
    t.integer "shares"
    t.integer "comments"
    t.datetime "facebookad_posted_at"
    t.string "facebookad_url"
    t.string "review_csv_url"
    t.string "facebook_page_url"
    t.string "product_page_url"
    t.decimal "product_rating"
    t.decimal "heroscore"
    t.decimal "facebook_female"
    t.decimal "facebook_male"
    t.boolean "active", default: true
    t.string "trends_keyword"
    t.string "amazonurl"
    t.string "ebayurl"
    t.string "youtubevideoadurl"
    t.string "oberloimporturl"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "admin"
    t.string "subscriptionid"
    t.boolean "subscribed", default: false
    t.string "customerid"
    t.datetime "subscribed_until"
    t.string "name"
    t.string "paypal"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "warm_products", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
